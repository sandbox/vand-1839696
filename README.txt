Introduction
============

Central Authentication Services (CAS) is a commonly used Single Sign-On
protocol used by many universities and large organizations. For a brief
introduction, please see the Jasig website: http://www.jasig.org/cas/about

CAS_Username_Link:
	This module hooks into the CAS module and links an existing
	Drupal username to the CAS username required by the CAS module.


Requirements
============
PHP 5 with the following modules:
  curl, openssl, dom, zlib, and xml
phpCAS version 1.0.0 or later.
CAS Drupal module - http://drupal.org/project/cas

Installation
============

* Place the cas_username_link folder in your Drupal modules directory.

* Go to Administer > Modules and enable the CAS Username Link module..

Configuration & Workflow
========================

This module will be invoked when a user logs in using the CAS system.
